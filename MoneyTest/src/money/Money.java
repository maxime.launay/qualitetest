package money;

import java.util.ArrayList;

public class Money {
	private int fAmount;
	private String fCurrency;
	static ArrayList<String> deviseISO = new ArrayList<String>() {{
		add("EUR");
		add("USD");
		add("CHF");
		add("GBP");
	}};

	
	public Money(int amount, String currency) {
		if ( (amount >= 0) && (deviseISO.contains(currency)) ) {
				fAmount = amount;
				fCurrency = currency;
			}
		}
		
	public int amount() {
		return fAmount;
	};
	
	public String currency() {
		return fCurrency;
	};
	
	public Money add(Money m) {
		if(fCurrency.equalsIgnoreCase(m.currency())) {
			fAmount += m.amount();
			return this;
		}else {
			return null;
		}
	};


	public Money add(int namout, String ncurrency) {
		if(fCurrency.equalsIgnoreCase(ncurrency)) {
			if (fAmount + namout > 0 ) {
				fAmount += namout;
				return this;
			}else {
				return null;
			}
		}else {
			return null;
		}
	};
	
	
}
