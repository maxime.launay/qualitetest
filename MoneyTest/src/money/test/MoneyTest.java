package money.test;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import money.Money;
import money.MoneyBag;

@DisplayName("Test JUnit 5")
public class MoneyTest {
	
	@BeforeAll
	static void beforeAll() {
		System.out.println("Avant tous les test");
		
	}
	

	@Test
	@DisplayName("JUnit Money Test Instanciation")
	void testMoney() {
		Money money1 = new Money(10, "EUR"); 
		assertTrue(money1.amount() == 10);
		assertTrue(money1.currency().equalsIgnoreCase("EUR"));
	}
	
	@Test
	@DisplayName("JUnit Money Test Instanciation valeur negative")
	void testMoney2() {
		Money money2 = new Money(-10, "GBP"); 
		assertTrue((money2.amount() == 0));
	}
	
	@Test
	@DisplayName("JUnit Money Test Instanciation valeur negative")
	void testMoney3() {
		Money money3 = new Money(10, "JPO"); 
		assertNull((money3.currency()));
	}
	
	
	@Test
	@DisplayName("JUnit Money Test Money Add meme devise")
	void testMoney4() {
		Money money4 = new Money(10, "EUR"); 
		Money money5 = new Money(20, "EUR");
		money4.add(money5);
		assertTrue(money4.amount() == 30);
	}
	
	@Test
	@DisplayName("JUnit Money Test Money Add devise diff")
	void testMoney5() {
		Money money6 = new Money(10, "EUR"); 
		Money money7 = new Money(20, "USD");
		assertNull(money6.add(money7));
	}
	
	@Test
	@DisplayName("JUnit Money Test Money Add")
	void testMoneyAdd() {
		Money moneyAdd = new Money(10, "EUR"); 
		moneyAdd.add(10, "EUR");
		assertTrue(moneyAdd.amount() == 20);
	}
	
	
	@Test
	@DisplayName("JUnit MoneyBag Test Instanciation")
	void testMoneyBag() {
		MoneyBag moneyBag1 = new MoneyBag(10, "EUR"); 
		assertTrue(moneyBag1.amount() == 10);
		assertTrue(moneyBag1.currency().equalsIgnoreCase("EUR"));
	}
	
	@Test
	@DisplayName("JUnit MoneyBag test add money")
	void testMoneyBagAddMoney() {
		MoneyBag moneyBagAdd = new MoneyBag(10, "EUR"); 
		Money money = new Money(10, "GBP"); 
		moneyBagAdd.add(money);
		assertTrue((moneyBagAdd.amount() == 20));
	}
	
	
	@Test
	@DisplayName("JUnit MoneyBag test add money bag")
	void testMoneyBagAddMoneyBag() {
		MoneyBag moneyBagAdd = new MoneyBag(10, "EUR"); 
		MoneyBag moneyBagAdd2 = new MoneyBag(10, "EUR"); 
		moneyBagAdd.add(moneyBagAdd2);
		assertTrue((moneyBagAdd.amount() == 20));
	}
	
	
	@Test
	@DisplayName("JUnit MoneyBag test subb money")
	void testMoneyBagSubMoney() {
		MoneyBag moneyBagSubb = new MoneyBag(10, "EUR"); 
		Money moneySubb = new Money(10, "GBP"); 
		moneyBagSubb.subb(moneySubb);
		assertTrue((moneyBagSubb.amount() == 0));
	}
	
	@Test
	@DisplayName("JUnit MoneyBag test subb money bag")
	void testMoneyBagSubbMoneyBag() {
		MoneyBag moneyBagSubb = new MoneyBag(30, "EUR"); 
		MoneyBag moneyBagSubb2 = new MoneyBag(10, "EUR"); 
		moneyBagSubb.subb(moneyBagSubb2);
		assertTrue((moneyBagSubb.amount() == 20));
	}
	
	@TestFactory
	Stream<DynamicTest> dynamicTestsFromIntStream() {
		List<Integer> inputs = Arrays.asList(20,30,40,-100);
		List<Integer> results = Arrays.asList(30,40,50,10);
	    return inputs.stream()
	      .map(n -> DynamicTest.dynamicTest("test " + n,
	        () -> {	
	        int i = inputs.indexOf(n);	
	        int res = results.get(i);
        	Money money1 = new Money(10, "EUR");
        	money1.add(n, "EUR");
        	assertTrue(money1.amount() == res);
	        }));
	}


	@AfterAll
	static void afterAll() {
		System.out.println("Apres tous les tests");
	}
	
}
