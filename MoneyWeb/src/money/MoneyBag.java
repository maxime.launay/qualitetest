package money;


public class MoneyBag {
	private int fAmount;
	private String fCurrency;


	public MoneyBag(int amount, String currency) {
		if ( (amount >= 0) ) {
				fAmount = amount;
				fCurrency = currency;
			}
	};
		
	public int amount() {
		return fAmount;
	};
	
	public String currency() {
		return fCurrency;
	};
	
	public MoneyBag add(Money m) {
		fAmount += m.amount();
		return this;
	};
	
	public MoneyBag add(MoneyBag mb) {
		fAmount += mb.amount();
		return this;
	};
	
	public MoneyBag subb(Money m) {
		fAmount = fAmount - m.amount();
		return this;
	};
	

	public MoneyBag subb(MoneyBag mb) {
		fAmount = fAmount - mb.amount();
		return this;
	};
	

}
