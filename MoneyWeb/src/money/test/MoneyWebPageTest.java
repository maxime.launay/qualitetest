package money.test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

class MoneyWebPageTest {
	@Test
	public void moneyWebTestAddEUR() {

		//System.setProperty("webdriver.firefox.driver", "/opt/local/bin/chromedriver");
		WebDriver browser = new FirefoxDriver();
		browser.get("http://localhost:8080/MoneyWeb/MoneyWeb.jsp");
		
		WebElement inputAmount1 = browser.findElement(By.name("amount1"));
		inputAmount1.sendKeys("10");
		WebElement inputAmount2 = browser.findElement(By.name("amount2"));
		inputAmount2.sendKeys("20");
		
		Select dropdown1 = new Select(browser.findElement(By.id("devise")));
		dropdown1.selectByValue("EUR");
		
		Select dropdown2 = new Select(browser.findElement(By.id("devise2")));
		dropdown2.selectByValue("EUR");
		
		WebElement submit_button = browser.findElement(By.xpath("//input[@value='add']"));
		submit_button.submit();

		
		try {
			Thread.sleep(10*100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		WebElement res = browser.findElement(By.name("res"));
		res.getAttribute("placeholder");

		assertTrue(res.getAttribute("placeholder").contentEquals("30"));
		
		browser.close();

	}
	
	@Test
	public void moneyWebTestAddDOL() {

		//System.setProperty("webdriver.firefox.driver", "/opt/local/bin/chromedriver");
		WebDriver browser = new FirefoxDriver();
		browser.get("http://localhost:8080/MoneyWeb/MoneyWeb.jsp");
		
		WebElement inputAmount1 = browser.findElement(By.name("amount1"));
		inputAmount1.sendKeys("100");
		WebElement inputAmount2 = browser.findElement(By.name("amount2"));
		inputAmount2.sendKeys("20");
		
		Select dropdown1 = new Select(browser.findElement(By.id("devise")));
		dropdown1.selectByValue("USD");
		
		Select dropdown2 = new Select(browser.findElement(By.id("devise2")));
		dropdown2.selectByValue("USD");
		
		WebElement submit_button = browser.findElement(By.xpath("//input[@value='add']"));
		submit_button.submit();

		
		try {
			Thread.sleep(10*100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		WebElement res = browser.findElement(By.name("res"));
		res.getAttribute("placeholder");

		assertTrue(res.getAttribute("placeholder").contentEquals("120"));
		
		browser.close();

	}
	
	@Test
	public void moneyWebTestAddGBP() {

		//System.setProperty("webdriver.firefox.driver", "/opt/local/bin/chromedriver");
		WebDriver browser = new FirefoxDriver();
		browser.get("http://localhost:8080/MoneyWeb/MoneyWeb.jsp");
		
		WebElement inputAmount1 = browser.findElement(By.name("amount1"));
		inputAmount1.sendKeys("1000");
		WebElement inputAmount2 = browser.findElement(By.name("amount2"));
		inputAmount2.sendKeys("3000");
		
		Select dropdown1 = new Select(browser.findElement(By.id("devise")));
		dropdown1.selectByValue("GBP");
		
		Select dropdown2 = new Select(browser.findElement(By.id("devise2")));
		dropdown2.selectByValue("GBP");
		
		WebElement submit_button = browser.findElement(By.xpath("//input[@value='add']"));
		submit_button.submit();

		
		try {
			Thread.sleep(10*100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		WebElement res = browser.findElement(By.name("res"));
		res.getAttribute("placeholder");

		assertTrue(res.getAttribute("placeholder").contentEquals("4000"));
		
		browser.close();

	}



}
