package money;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class moneyServlet
 */
@WebServlet("/moneyServlet")
public class moneyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public moneyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int amount1 = Integer.parseInt(request.getParameter("amount1"));
        int amount2 = Integer.parseInt(request.getParameter("amount2"));
        String devise = request.getParameter("devise");
        String devise2 = request.getParameter("devise2");

        
        Money money1 = new Money(amount1,devise); 
		Money money2 = new Money(amount2,devise2);
		money1.add(money2);
        
        request.setAttribute("res", money1.amount());
		this.getServletContext().getRequestDispatcher( "/MoneyWeb.jsp" ).forward( request, response);
	}

}
